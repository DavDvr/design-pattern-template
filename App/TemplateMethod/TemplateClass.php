<?php


namespace Template\App\TemplateMethod;

abstract class TemplateClass
{
    /**
     * Squelette d'un algorithme
     * Je laisse compléter cet algorithme par les classes qui implémenteront la classe TemplateClass
     * @return float
     */
    public function templateMethod(): float
    {
        $n = $this->operation1();
        $somme = 0;
        for ($i = 0; $i < $n; $i++){
            $somme+= $this->operation2($i);
        }
        return $somme;
    }

    protected abstract function operation1(): int;
    protected abstract function operation2(int $nbr): int;
}