<?php

namespace Template\App\TemplateMethod;

/**
 * Class Implementation1 implémente TemplateClass
 * @package Template\App
 */
class Implementation1 extends TemplateClass
{

    protected function operation1(): int
    {
        return 10;
    }

    protected function operation2(int $nbr): int
    {
        return 20;
    }
}