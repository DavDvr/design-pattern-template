<?php

namespace Template\App\TemplateMethod;

/**
 * Class Implementation2 implémente TemplateClass
 * @package Template\App\TemplateMethod
 */
class Implementation2 extends TemplateClass
{

    protected function operation1(): int
    {
        return 8;
    }

    protected function operation2(int $nbr): int
    {
        return 9;
    }
}