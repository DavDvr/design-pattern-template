<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Template</title>
</head>
<body>
<?php


use Template\App\TemplateMethod\Implementation1;
use Template\App\TemplateMethod\Implementation2;
use Template\App\TemplateMethod\TemplateClass;
require_once(dirname(__DIR__)."/vendor/autoload.php");


echo "__________Implementation1________<br>";
$template = TemplateClass::class;
$template = new Implementation1();
echo $template->templateMethod();
echo"<br>";
echo "__________Implementation2________<br>";
$template = new Implementation2();
echo $template->templateMethod();

?>
</body>
</html>
